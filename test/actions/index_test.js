import { expect } from '../test_helper';
import { SAVE_COMMENTS } from '../../src/actions/types';
import { saveComments } from '../../src/actions';
describe('actions' , () => {

    describe('save Comments' , () => {


        it('has the correct type', () => {
            const action = saveComments();
            expect(action.type).to.equal(SAVE_COMMENTS);
          });
        
          
          it('has the correct payload', () => {
            const action = saveComments('new comment');
            expect(action.payload).to.equal('new comment');
          });

    });

});