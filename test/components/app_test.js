import { renderComponent , expect } from '../test_helper';
import App from '../../src/components/app';
import CommentList from '../../src/components/comment_list';
import CommentBox from '../../src/components/comment_box';

describe('App' , () => {
  let component;

  beforeEach(() => {
    component = renderComponent(App);
  });

  it('shows comment box', () => {
    component = renderComponent(CommentBox);
    expect(component).to.have.class('comment_box');
  });

  
  it('shows comment list', () => {
    component = renderComponent(CommentList);
    expect(component).to.have.class('comment_list');
  });



});
