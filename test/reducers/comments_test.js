import { expect } from '../test_helper';
import commentReducer from '../../src/reducers/comments';
import { SAVE_COMMENTS } from '../../src/actions/types';

describe('Comments Reducer' , () => {

  it('handles action with unknown types', () => {
    expect(commentReducer(undefined,{})).to.eql([]);
  });

  it('handles action of type SAVE_COMMENTS', () => {
    const action = {type: SAVE_COMMENTS, payload: 'this is a comment'};
    expect(commentReducer( [] , action )).to.eql(['this is a comment']);
  });

});