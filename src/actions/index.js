import { SAVE_COMMENTS } from './types';

export function saveComments(comments) {
    return {
        type: SAVE_COMMENTS,
        payload: comments
    }

}